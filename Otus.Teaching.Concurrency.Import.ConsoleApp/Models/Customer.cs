﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.ConsoleApp
{
    public partial class Customer
    {
        public override string ToString()
        {
            return $"Customer: Id={Id}, FullName={FullName}, Email={Email}, Phone={Phone}";
        }
    }
}
