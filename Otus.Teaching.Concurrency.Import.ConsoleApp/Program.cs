﻿using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using System;
using System.Configuration;
using System.Net.Http;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.ConsoleApp
{
    class Program
    {
        private static string BASE_URL;

        static async Task Main(string[] args)
        {
            BASE_URL = ConfigurationManager.AppSettings["BASE_URL"];

            await FindCustomer();

            await AddRandomCustomer();

            Console.ReadKey();
        }

        static async Task FindCustomer()
        {
            try
            {
                Console.Write("Enter Customer Id: ");
                if (int.TryParse(Console.ReadLine(), out int id))
                {
                    Customer customer = await CustomerGETAsync(id);

                    Console.WriteLine(customer);
                }
                else
                {
                    Console.WriteLine("Wrong id");
                }
            }
            catch (ApiException ex)
            {
                if (ex.StatusCode == 404)
                {
                    Console.WriteLine("Customer not found");
                }
            }
        }

        static async Task AddRandomCustomer()
        {
            Customer customer = GetRandomCustomer();

            try
            {
                customer = await CustomerPOSTAsync(customer);

                Console.WriteLine(customer);
                Console.WriteLine("Customer has been successfully created");
            }
            catch (ApiException ex)
            {
                if (ex.StatusCode == 409)
                {
                    Console.WriteLine($"Customer with id={customer.Id} already exists");
                }
            }
        }

        static Customer GetRandomCustomer()
        {
            var customers = RandomCustomerGenerator.Generate(1);
            var customer = customers[0];

            return new Customer()
            {
                Email = customer.Email,
                FullName = customer.FullName,
                Id = customer.Id,
                Phone = customer.Phone
            };
        }

        static async Task<Customer> CustomerGETAsync(int id)
        {
            using (var httpClient = new HttpClient())
            {
                var client = new swaggerClient(BASE_URL, httpClient);
                return await client.CustomerGETAsync(id);
            }
        }

        static async Task<Customer> CustomerPOSTAsync(Customer customer)
        {
            using (var httpClient = new HttpClient())
            {
                var client = new swaggerClient(BASE_URL, httpClient);
                return await client.CustomerPOSTAsync(customer);
            }
        }
    }
}
