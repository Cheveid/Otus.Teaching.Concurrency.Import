using Otus.Teaching.Concurrency.Import.Handler.Data;
using XmlDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.XmlGenerator;
using CsvDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.CsvGenerator;

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    public static class GeneratorFactory
    {
        public static string[] dataTypes = new string[] { "xml", "csv" };

        public static IDataGenerator GetGenerator(string fileName, int dataCount, string dataType)
        {
            return dataType switch
            {
                "xml" => new XmlDataGenerator(fileName, dataCount),
                "csv" => new CsvDataGenerator(fileName, dataCount),
                _ => null
            };
        }
    }
}