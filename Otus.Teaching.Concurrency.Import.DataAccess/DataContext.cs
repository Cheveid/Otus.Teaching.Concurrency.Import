﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public class DataContext: DbContext
    {
        public DbSet<Customer> Customers { get; set; }

        public DataContext()
        {
            Database.EnsureCreated();
        }

        public void ClearDb()
        {
            //Customers.RemoveRange(Customers);
#pragma warning disable CS0618 // Тип или член устарел
            Database.ExecuteSqlCommand("TRUNCATE TABLE public.\"Customers\"");
#pragma warning restore CS0618 // Тип или член устарел
            SaveChanges();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Username=CustomersDB;Password=CustomersDB;Database=CustomersDB");
        }
    }
}
