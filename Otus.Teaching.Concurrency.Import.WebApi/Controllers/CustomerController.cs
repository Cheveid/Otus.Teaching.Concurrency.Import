﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.WebApi.Controllers
{
    [Route("api/customer")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly DataContext _dataContext;
        private readonly ICustomerRepository _customerRepository;

        public CustomerController(DataContext dataContext, ICustomerRepository customerRepository)
        {
            _dataContext = dataContext;
            _customerRepository = customerRepository;
        }

        // GET api/customer/1
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Customer>> Get(int id)
        {
            Customer customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            return Ok(customer);
        }

        // POST api/customer
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<ActionResult<Customer>> Post([FromBody] Customer customer)
        {
            if (await _customerRepository.GetByIdAsync(customer.Id) != null)
            {
                return Conflict();
            }

            _customerRepository.AddCustomer(customer);
            await _dataContext.SaveChangesAsync();
            
            return Ok(customer);
        }
    }
}
