using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Handler.Repositories
{
    public interface ICustomerRepository
    {
        void AddCustomer(Customer customer);
        Task<Customer> GetByIdAsync(int id);
    }
}